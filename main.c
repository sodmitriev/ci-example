#include "stdio.h"

int main()
{
    FILE* file = fopen("log.txt", "a");
    if(file == NULL)
    {
        fprintf(stderr, "%s\n", "Failed to open log");
        return 1;
    }
    fprintf(file, "%s\n%s\n", "I am working...", "Done!");
    return 0;
}
